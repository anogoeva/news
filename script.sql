DROP DATABASE IF EXISTS news;

CREATE DATABASE IF NOT EXISTS news;

use news;

CREATE TABLE IF NOT EXISTS news (
    id int not null  auto_increment primary key ,
    title varchar(255) not null,
    description text not null,
    image varchar(255) null,
    date timestamp
);

CREATE TABLE IF NOT EXISTS comment (
    id int not null auto_increment primary key,
    news_id int null,
    author varchar(255) null,
    comment text not null,
    constraint comment_news_id_fk
    foreign key(news_id)
    references news(id)
    on update cascade
    on delete CASCADE
);

INSERT INTO news (title, description, image, date)
VALUES ('Прогноз погоды', 'В КР меняется погода', null, '2019-07-02 06:14:00.742000000'),
       ('Политическая обстановка', 'В КР меняются депутаты', null, '2019-07-02 06:14:00.742000000'),
       ('Благотворительность', 'В КР открылся приют', null, '2019-07-02 06:14:00.742000000'),
       ('Шоу бизнес', 'В КР много поп звезд', null, '2019-07-02 06:14:00.742000000'),
       ('Медицина без комментариев', 'В КР много поп больных', null, '2019-07-02 06:14:00.742000000');

INSERT INTO comment (news_id, author, comment)
VALUES (1, 'Стал', 'Отличная новость'),
       (2, 'Айжан', 'Ужасная новость'),
       (3, 'Амани', 'Как классно!'),
       (4, 'Али', 'Ура!');

SELECT * FROM news;
SELECT * FROM comment;



