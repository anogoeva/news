import React, {useEffect} from 'react';
import {useDispatch, useSelector} from "react-redux";
import {createComments, fetchCommentsByNews, fetchSingleNews} from "../../store/actions/newsActions";
import {Box, Paper, Typography} from "@material-ui/core";
import imageNotAvailable from '../../assets/images/not_available.png';
import {apiURL} from "../../config";
import CommentForm from "../CommentForm/CommentForm";

const SingleNews = ({match}) => {
    const dispatch = useDispatch();
    const singleNews = useSelector(state => state.news.singleNews);
    const comments = useSelector(state => state.news.comments);
    let cardImage = imageNotAvailable;

    useEffect(() => {
        dispatch(fetchSingleNews(match.params.id));
        dispatch(fetchCommentsByNews(match.params.id));
    }, [dispatch, match.params.id]);

    const onSubmit = async commentData => {
        await dispatch(createComments(commentData));
    };

    return singleNews && (
        <Paper component={Box} p={2}>
            <Typography variant="h4">{singleNews.title}</Typography>
            <Typography variant="body1">{singleNews.description}</Typography>
            <Paper variant="outlined">
                <img src={singleNews.image ? apiURL + '/uploads/' + singleNews.image : cardImage} alt="img"/>
            </Paper>
            <Typography variant="subtitle2">{singleNews.date} </Typography>


            {comments ? comments.map(comment => (
                <Paper variant="outlined" key={comment.id}>
                    <Typography variant="subtitle2">
                        Author: <i>{comment.author}</i>
                    </Typography>
                    <Typography variant="subtitle2">
                        Comment: <b>{comment.comment}</b>
                    </Typography>
                </Paper>
            )) : comments}
            <CommentForm onSubmit={onSubmit}/>
        </Paper>
    );
};

export default SingleNews;