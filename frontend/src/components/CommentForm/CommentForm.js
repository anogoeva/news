import React, {useState} from "react";
import {makeStyles} from "@material-ui/core/styles";
import Button from "@material-ui/core/Button";
import Grid from "@material-ui/core/Grid";
import TextField from "@material-ui/core/TextField";
import {Typography} from "@material-ui/core";
import {useSelector} from "react-redux";

const useStyles = makeStyles(theme => ({
    root: {
        marginTop: theme.spacing(2)
    },
}));

const CommentForm = ({onSubmit}) => {

    const singleNews = useSelector(state => state.news.singleNews);

    const classes = useStyles();

    const [state, setState] = useState({
        author: "",
        comment: ""
    });

    const submitFormHandler = e => {
        e.preventDefault();
        const object = {};
        object.news_id = singleNews.id;
        object.author = state.author;
        object.comment = state.comment;
        onSubmit(object);
        setState({
            author: '',
            comment: ''
        })
    };

    const inputChangeHandler = e => {
        const name = e.target.name;
        const value = e.target.value;
        setState(prevState => {
            return {...prevState, [name]: value};
        });
    };

    return (
        <Grid
            container
            direction="column"
            spacing={2}
            component="form"
            className={classes.root}
            autoComplete="off"
            onSubmit={submitFormHandler}
        >
            <Typography variant="h4" >
               Add new comment
            </Typography>
            <Grid item xs>
                <TextField
                    label="Name"
                    name="author"
                    value={state.author}
                    onChange={inputChangeHandler}
                />
            </Grid>

            <Grid item xs>
                <TextField
                    required
                    multiline
                    rows={3}
                    label="Comment"
                    name="comment"
                    value={state.comment}
                    onChange={inputChangeHandler}
                />
            </Grid>

            <Grid item xs>
                <Button type="submit" color="primary" variant="contained">Add</Button>
            </Grid>
        </Grid>
    );
};

export default CommentForm;