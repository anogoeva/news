import {Link} from "react-router-dom";
import {
    Card,
    CardActions,
    CardContent,
    CardHeader,
    CardMedia,
    Grid,
    IconButton,
    makeStyles,
    Typography
} from "@material-ui/core";
import imageNotAvailable from '../../assets/images/not_available.png';
import {apiURL} from "../../config";
import Button from "@material-ui/core/Button";
import {deleteNews} from "../../store/actions/newsActions";
import {useDispatch} from "react-redux";

const useStyles = makeStyles({
    card: {
        height: '100%'
    },
    media: {
        height: 0,
        paddingTop: '56.25%'
    }
});

const NewsItem = ({id, title, image, date}, props) => {

    const dispatch = useDispatch();
    const removeNewsHandler = id => {
        dispatch(deleteNews(id));
    };

    const classes = useStyles();

    let cardImage = imageNotAvailable;

    if (image) {
        cardImage = apiURL + '/uploads/' + image;
    }

    return (
        <Grid item xs={12} sm={6} md={6} lg={4}>
            <Card className={classes.card}>
                <CardHeader title={title}/>
                <CardMedia
                    image={cardImage}
                    title={title}
                    className={classes.media}
                />
                <CardContent>
                    <Typography variant="subtitle1">
                        Дата публикации: <i>{date}</i>
                    </Typography>
                </CardContent>
                <CardActions>
                    <IconButton component={Link} to={'/news/' + id}>
                        <Typography variant="subtitle1">
                            READ FULL POST
                        </Typography>
                    </IconButton>
                    <Button onClick={() => removeNewsHandler(id)}>DELETE</Button>
                </CardActions>
            </Card>
        </Grid>
    );
};

export default NewsItem;