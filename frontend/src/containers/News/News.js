import React, {useEffect} from 'react';
import {Button, Grid, Typography} from "@material-ui/core";
import {useDispatch, useSelector} from "react-redux";
import {Link} from "react-router-dom";
import {fetchNews} from "../../store/actions/newsActions";
import NewsItem from "../../components/NewsItem/NewsItem";

const News = () => {
    const dispatch = useDispatch();
    const news = useSelector(state => state.news.news);

    useEffect(() => {
        dispatch(fetchNews());
    }, [dispatch]);



    return (
        <Grid container direction="column" spacing={2}>
            <Grid item container justifyContent="space-between" alignItems="center">
                <Grid item>
                    <Typography variant="h4">Posts</Typography>
                </Grid>
                <Grid item>
                    <Button color="primary" component={Link} to="/post/new">Add new post</Button>
                </Grid>
            </Grid>
            <Grid item container direction="column" alignItems="center" spacing={1}>
                {news.map(news => (
                    <NewsItem
                        key={news.id}
                        id={news.id}
                        title={news.title}
                        image={news.image}
                        date={news.date}
                    />
                ))}
            </Grid>
        </Grid>
    );
};

export default News;