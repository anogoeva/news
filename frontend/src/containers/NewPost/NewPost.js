import React from "react";
import Typography from "@material-ui/core/Typography";
import {useDispatch} from "react-redux";
import NewPostForm from "../../components/NewPostForm/NewPostForm";
import {createPost} from "../../store/actions/newsActions";

const NewPost = ({history}) => {
  const dispatch = useDispatch();

  const onSubmit = async postData => {
    await dispatch(createPost(postData));
    history.replace('/');
  };

  return (
    <>
      <Typography variant="h4">New Post</Typography>
      <NewPostForm
        onSubmit={onSubmit}
      />
    </>
  );
};

export default NewPost;