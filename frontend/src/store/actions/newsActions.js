import axios from "axios";

export const FETCH_ALL_NEWS_REQUEST = 'FETCH_ALL_NEWS_REQUEST';
export const FETCH_ALL_NEWS_SUCCESS = 'FETCH_ALL_NEWS_SUCCESS';
export const FETCH_ALL_NEWS_FAILURE = 'FETCH_ALL_NEWS_FAILURE';

export const FETCH_NEWS_REQUEST = 'FETCH_NEWS_REQUEST';
export const FETCH_NEWS_SUCCESS = 'FETCH_NEWS_SUCCESS';
export const FETCH_NEWS_FAILURE = 'FETCH_NEWS_FAILURE';

export const DELETE_NEWS_REQUEST = 'DELETE_NEWS_REQUEST';
export const DELETE_NEWS_SUCCESS = 'DELETE_NEWS_SUCCESS';
export const DELETE_NEWS_FAILURE = 'DELETE_NEWS_FAILURE';

export const CREATE_NEWS_REQUEST = 'CREATE_NEWS_REQUEST';
export const CREATE_NEWS_SUCCESS = 'CREATE_NEWS_SUCCESS';
export const CREATE_NEWS_FAILURE = 'CREATE_NEWS_FAILURE';

export const CREATE_COMMENTS_REQUEST = 'CREATE_COMMIT_REQUEST';
export const CREATE_COMMENTS_SUCCESS = 'CREATE_COMMIT_SUCCESS';
export const CREATE_COMMENTS_FAILURE = 'CREATE_COMMIT_FAILURE';

export const FETCH_COMMENTS_REQUEST = 'FETCH_COMMENTS_REQUEST';
export const FETCH_COMMENTS_SUCCESS = 'FETCH_COMMENTS_SUCCESS';
export const FETCH_COMMENTS_FAILURE = 'FETCH_COMMENTS_FAILURE';

export const fetchAllNewsRequest = () => ({type: FETCH_ALL_NEWS_REQUEST});
export const fetchAllNewsSuccess = news => ({type: FETCH_ALL_NEWS_SUCCESS, payload: news});
export const fetchAllNewsFailure = () => ({type: FETCH_ALL_NEWS_FAILURE});

export const fetchNewsRequest = () => ({type: FETCH_NEWS_REQUEST});
export const fetchNewsSuccess = news => ({type: FETCH_NEWS_SUCCESS, payload: news});
export const fetchNewsFailure = () => ({type: FETCH_NEWS_FAILURE});

export const deleteNewsRequest = () => ({type: DELETE_NEWS_REQUEST});
export const deleteNewsSuccess = (id) => ({type: DELETE_NEWS_SUCCESS, payload: id});
export const deleteNewsFailure = () => ({type: DELETE_NEWS_FAILURE});

export const createNewsRequest = () => ({type: CREATE_NEWS_REQUEST});
export const createNewsSuccess = () => ({type: CREATE_NEWS_SUCCESS});
export const createNewsFailure = () => ({type: CREATE_NEWS_FAILURE});

export const createCommentsRequest = () => ({type: CREATE_COMMENTS_REQUEST});
export const createCommentsSuccess = (commentData) => ({type: CREATE_COMMENTS_SUCCESS, payload: commentData});
export const createCommentsFailure = () => ({type: CREATE_COMMENTS_FAILURE});

export const fetchCommentsRequest = () => ({type: FETCH_COMMENTS_REQUEST});
export const fetchCommentsSuccess = news => ({type: FETCH_COMMENTS_SUCCESS, payload: news});
export const fetchCommentsFailure = () => ({type: FETCH_COMMENTS_FAILURE})

export const fetchNews = () => {
  return async dispatch => {
    try {
      dispatch(fetchAllNewsRequest());
      const response = await axios.get('http://localhost:8000/news');
      dispatch(fetchAllNewsSuccess(response.data));
    } catch (e) {
      dispatch(fetchAllNewsFailure());
    }
  };
};

export const fetchSingleNews = id => {
  return async dispatch => {
    try {
      dispatch(fetchNewsRequest());
      const response = await axios.get('http://localhost:8000/news/' + id);
      dispatch(fetchNewsSuccess(response.data));
    } catch (e) {
      dispatch(fetchNewsFailure());
    }
  };

};

export const deleteNews = (id) => {
  return async dispatch => {
    try {
      dispatch(deleteNewsRequest());
      const response = await axios.delete('http://localhost:8000/news/' + id);
      if (response.data.message) {
        dispatch(deleteNewsSuccess(id));
      }
    } catch (e) {
      dispatch(deleteNewsFailure());
    }
  };
};

export const fetchCommentsByNews = id => {
  return async dispatch => {
    try {
      dispatch(fetchCommentsRequest());
      const response = await axios.get('http://localhost:8000/comments?news_id=' + id);
      dispatch(fetchCommentsSuccess(response.data));
    } catch (e) {
      dispatch(fetchCommentsFailure());
    }
  };

};

export const createPost = postData => {
  return async dispatch => {
    try {
      dispatch(createNewsRequest());
      await axios.post('http://localhost:8000/news', postData);
      dispatch(createNewsSuccess());
    } catch (e) {
      dispatch(createNewsFailure());
      throw e;
    }
  };
};

export const createComments = commentData => {
  return async dispatch => {
    try {
      dispatch(createCommentsRequest());
      const response = await axios.post('http://localhost:8000/comments', commentData);
      dispatch(createCommentsSuccess(response.data));
    } catch (e) {
      dispatch(createCommentsFailure());
      throw e;
    }
  };
};