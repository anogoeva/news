import {
    CREATE_COMMENTS_FAILURE,
    CREATE_COMMENTS_REQUEST, CREATE_COMMENTS_SUCCESS,
    DELETE_NEWS_FAILURE,
    DELETE_NEWS_REQUEST,
    DELETE_NEWS_SUCCESS,
    FETCH_ALL_NEWS_FAILURE,
    FETCH_ALL_NEWS_REQUEST,
    FETCH_ALL_NEWS_SUCCESS, FETCH_COMMENTS_FAILURE,
    FETCH_COMMENTS_REQUEST,
    FETCH_COMMENTS_SUCCESS,
    FETCH_NEWS_FAILURE,
    FETCH_NEWS_REQUEST,
    FETCH_NEWS_SUCCESS

} from "../actions/newsActions";

const initialState = {
    fetchLoading: false,
    singleLoading: false,
    news: [],
    singleNews: null,
    comments: null
};

const newsReducer = (state = initialState, action) => {
    switch (action.type) {
        case FETCH_ALL_NEWS_REQUEST:
            return {...state, fetchLoading: true};
        case FETCH_ALL_NEWS_SUCCESS:
            return {...state, fetchLoading: false, news: action.payload};
        case FETCH_ALL_NEWS_FAILURE:
            return {...state, fetchLoading: false};
        case FETCH_NEWS_REQUEST:
          return {...state, singleLoading: true};
        case FETCH_NEWS_SUCCESS:
          return {...state, singleLoading: false, singleNews: action.payload};
        case FETCH_NEWS_FAILURE:
          return {...state, singleLoading: false};
        case DELETE_NEWS_REQUEST:
            return {...state, singleLoading: true};
        case DELETE_NEWS_SUCCESS:
            return {...state};
        case DELETE_NEWS_FAILURE:
            return {...state, singleLoading: false};
        case FETCH_COMMENTS_REQUEST:
            return {...state, singleLoading: true};
        case FETCH_COMMENTS_SUCCESS:
            return {...state, comments: action.payload};
        case FETCH_COMMENTS_FAILURE:
            return {...state, singleLoading: false};
        case CREATE_COMMENTS_REQUEST:
            return {...state, singleLoading: true};
        case CREATE_COMMENTS_SUCCESS:
            return {...state, comments: [...state.comments, action.payload]};
        case CREATE_COMMENTS_FAILURE:
            return {...state, singleLoading: false};
        default:
            return state;
    }
};

export default newsReducer;