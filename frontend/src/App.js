import {Route, Switch} from "react-router-dom";
import Layout from "./components/UI/Layout/Layout";
import News from "./containers/News/News";
import SingleNews from "./components/SingleNews/SingleNews";
import NewPost from "./containers/NewPost/NewPost";


const App = () => (
    <Layout>
      <Switch>
        <Route path="/" exact component={News}/>
        <Route path="/post/new" component={NewPost}/>
        <Route path="/news/:id" component={SingleNews}/>
      </Switch>
    </Layout>
);

export default App;
