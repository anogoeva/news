const express = require('express');
const mysqlDb = require('../mysqlDb');


const router = express.Router();

router.get('/', async (req, res) => {
  if (req.query.news_id) {
    let newsId = req.query.news_id;
    const [comments] = await mysqlDb.getConnection().query('SELECT * FROM ?? where news_id = ?', ['comment', newsId]);
    res.send(comments);
  } else {
    const [comments] = await mysqlDb.getConnection().query('SELECT * FROM ??', ['comment']);
    res.send(comments);
  }
});

router.post('/', async (req, res) => {

   if (!req.body.news_id || !req.body.comment) {
    return res.status(400).send({error: 'Data not valid in comments'});
  }

  const comment = {
    news_id: req.body.news_id,
    author: req.body.author ? req.body.author : 'anonymous',
    comment: req.body.comment
  };

  const [newsExact] = await mysqlDb.getConnection().query('SELECT * FROM ?? where id = ?', ['news', req.body.news_id]);
  if (newsExact[0]) {
    const [newComment] = await mysqlDb.getConnection().query(
      'INSERT INTO ?? (news_id, author, comment) values (?, ?, ?)',
      ['comment', comment.news_id, comment.author, comment.comment]
    );
    res.send({
      ...comment,
      id: newComment.insertId
    })
  } else {
    return res.status(400).send({error: 'News not found!'});
  }
});

router.delete('/:id', async (req, res) => {
  const id = req.params.id;
  try {
    const [newQuery] = await mysqlDb.getConnection().query(
      'DELETE FROM ?? where id = ?',
      ['comment', id]);
    if (newQuery.affectedRows === 1) {
      res.send({message: 'Successfully deleted ' + newQuery.affectedRows + ' row with id ' + id});
    }
  } catch (error) {
    res.send({error: 'Unable to delete row with id ' + id + ', because there is a related entry.'});
  }
});

module.exports = router;
